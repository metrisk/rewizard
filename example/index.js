const path = require('path')
const webpack = require('webpack')
const env = process.argv[2] ? process.argv[2].replace('--', '') : 'prod'
let webpackConfig = null

/**
 * Apply environment specific options
 */
switch (env) {
  case 'local':
    webpackConfig = require(path.resolve(__dirname, './webpack.config.js'))({
      env: 'development',
      entry: path.resolve(__dirname, './src/Index.tsx')
    })
    break;
  default:
    webpackConfig = require(path.resolve(__dirname, './webpack.config.js'))({
      env: 'production',
      entry: path.resolve(__dirname, './src/Index.tsx')
    })
}

/**
 * Build our bundles or start the development server
 */
if (env === 'local') {
  const Server = require('webpack-dev-server');
  const HOST = 'localhost'
  const PORT = 3002
  new Server(webpack(webpackConfig), {
    historyApiFallback: { index: './index.html' },
    contentBase: path.join(__dirname, 'dist'),
    open: true,
    port: PORT,
    stats: {
      warnings: false,
      colors: true
    }
  }).listen(PORT, HOST, (err) => {
    if (err) console.error(err);
  })
}
else {
  webpack(webpackConfig, (err, stats) => {
    if (err) { console.error(err); return }
    console.log(stats.toString({
      chunks: false,
      colors: true,
      builtAt: true,
      warnings: false
    }))
    console.log(`Webpack bundles created.`)
  });
}