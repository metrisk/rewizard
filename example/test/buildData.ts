/**
 * Some sample data to test a preloaded wizard with
 */
const sampleData: any = {
  name: 'Bob',
  'last-name': 'Smith',
  animal: 'elephant',
  size: 'x-large',
  colour: 'brown',
  temperament: 'mad',
  cost: '1000',
  notes: 'N/a'
}

export default sampleData
