// Components
import Input from '../components/Input/Input'
import Select from '../components/Select/Select'

/**
 * An example validation function you can pass in via the validation property
 *
 * @param {Object} e
 * The data object that gets passed in
 */
const text = (e: { value: string | boolean | string[] | number }): boolean =>
  typeof e.value === 'string' && e.value.length > 0 ? true : false

/**
 * A sample set of fields to get you up and running 😇
 */
const fields = [
  {
    id: 'name', // Required.
    component: Input, // Required.
    props: {
      // These props will be accessible as normal in your component.
      className: 'myclass',
      label: 'Name'
    },
    validation: {
      // Validation for your field
      fn: text, // Pass the returned Wizard data object to this function, accessible via first arg.
      errorMsg: 'Error', // This error message will be accessible in your component as a child of the rewizard prop.
      successMsg: 'Success' // Same as the error but passed in when valid.
    }
  },
  {
    id: 'hidden',
    component: Input,
    value: 'I am hidden',
    hidden: true // This field can still have a value, but isn't included in the main data object or rendered in the UI
  },
  {
    id: 'last-name',
    component: Input,
    props: {
      label: 'Last Name'
    },
    optional: true // Sets the field as always valid, i.e the value is allowed to be anything, including null or undefined.
  },
  {
    id: 'animal',
    component: Select,
    props: {
      label: 'Animal'
    },
    options: [
      { value: 'none', label: 'None', invalidate: ['size', 'colour', 'temperament'] }, // If this option is chosen, exclude 'size', 'colour' and 'temperament' from the Wizard data object.
      { value: 'cat', label: 'Cat' },
      { value: 'horse', label: 'Horse' },
      { value: 'snake', label: 'Snake' },
      { value: 'elephant', label: 'Elephant', invalidate: ['extra'] },
      { value: 'dog', label: 'Dog', invalidate: ['temperament'] }
    ]
  },
  {
    id: 'horse-details',
    component: Input,
    props: {
      label: 'Horse Details'
    }
  },
  {
    id: 'cat-details',
    component: Input,
    props: {
      label: 'Cat Details'
    }
  },
  {
    id: 'size',
    component: Select,
    props: {
      label: 'Size'
    },
    options: [
      { value: 'tiny', label: 'Tiny', prereq: { fieldId: 'animal', value: /(cat|snake)/ } }, // Only pass this option into the array if 'cat' or 'snake' has been chosen.
      { value: 'small', label: 'Small', prereq: { fieldId: 'animal', value: /(cat|snake|dog)/ } },
      { value: 'medium', label: 'Medium' },
      { value: 'large', label: 'Large', prereq: { fieldId: 'animal', value: /horse/ } },
      { value: 'x-large', label: 'Extra Large', prereq: { fieldId: 'animal', value: /(horse|elephant)/ } }
    ]
  },
  {
    id: 'colour',
    component: Select,
    props: {
      label: 'Colour'
    },
    options: [
      { value: 'tabby', label: 'Tabby', prereq: { fieldId: 'size', value: 'tiny' } },
      { value: 'green', label: 'Green', prereq: { fieldId: 'animal', value: 'snake' } },
      { value: 'brown', label: 'Brown' }
    ]
  },
  {
    id: 'temperament',
    component: Select,
    props: {
      label: 'Temperament'
    },
    options: [
      { value: 'good', label: 'Good', prereq: { fieldId: 'animal', value: 'cat' }, invalidate: ['cost'] },
      { value: 'mad', label: 'Mad' },
      { value: 'average', label: 'Average' }
    ]
  },
  {
    id: 'cost',
    component: Input,
    props: {
      label: 'Cost'
    },
    validation: {
      fn: text
    }
  },
  {
    id: 'notes',
    component: Input,
    props: {
      label: 'Notes'
    },
    prereq: { fieldId: 'cost', value: /\.*/ }, // Prerequirements work directly on the field as well, i.e this field will only show if the conditions are met.
    validation: {
      fn: text
    }
  }
]

export default fields
