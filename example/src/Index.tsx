import * as React from 'react'
import * as ReactDOM from 'react-dom'

// Components
import Wizard from 'rewizard'

// Content
import steps from './content/sampleSteps'
import fields from './content/sampleFields'
import data from './content/sampleData'

class App extends React.Component<any> {
  state: any = {
    data: null
  }

  componentWillMount = () => {
    const { savedData } = this.props
    this.setState({
      data: savedData
    })
  }

  saveData = (data: any) => {
    console.log('Saved!')
    this.setState({
      data: data.data
    })
  }

  render() {
    const { data } = this.state
    const config: any = {
      onCancel: (e: any) => alert('CANCEL:' + JSON.stringify(e)),
      onSave: (e: any) => this.saveData(e),
      onSubmit: (e: any) => alert('SUBMIT:' + JSON.stringify(e))
    }
    data && (config.savedData = data)
    return <Wizard config={config} steps={steps} fields={fields} />
  }
}

ReactDOM.render(<App savedData={data} />, document.getElementById('app'))
