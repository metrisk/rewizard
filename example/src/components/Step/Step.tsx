import * as React from 'react'

const Step: React.SFC<any> = (props) => {
  const { rewizard, children } = props
  const { id, state, actions } = rewizard
  const { valid, prev, next } = state
  const { onCancel, onSave, onSubmit } = actions

  const style = {
    padding: '5rem'
  }

  const btn = {
    display: 'inline-block',
    marginTop: '1rem',
    padding: '.5rem 1rem',
    border: 'none',
    borderRadius: '5px',
    textTransform: 'uppercase' as 'uppercase',
    fontFamily: 'sans-serif',
    fontSize: '.75rem',
    backgroundColor: '#6f88dd',
    color: 'white',
    textDecoration: 'none'
  }

  const disabled = valid === false && {
    opacity: 0.5,
    pointerEvents: 'none' as 'none'
  }

  return (
    <section id={id} style={style}>
      <div>{children}</div>
      {prev && (
        <a style={btn} href={`#${prev}`}>
          Prev
        </a>
      )}
      {onSave && (
        <button style={{ ...btn, marginRight: '1rem' }} onClick={onSave}>
          Save
        </button>
      )}
      {onSubmit && (
        <button style={btn} onClick={onSubmit}>
          Submit
        </button>
      )}
    </section>
  )
}

export default Step
