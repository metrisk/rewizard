import * as React from 'react'

const Select: React.SFC<any> = (props) => {
  const { rewizard, label, onChange } = props
  const { state } = rewizard
  const { value, options } = state

  const style = {
    height: '2rem',
    marginRight: '1rem',
    marginBottom: '1rem'
  }

  return (
    <div>
      <label style={{ display: 'block', fontFamily: 'sans-serif' }}>{label}</label>
      <select style={style} value={value} onChange={(e) => onChange(e.target.value)}>
        {!value && (
          <option disabled value="null">
            Select
          </option>
        )}
        {options &&
          options.map((x: any) => (
            <option key={`option-${x.value}`} value={x.value}>
              {x.label}
            </option>
          ))}
      </select>
    </div>
  )
}

export default Select
