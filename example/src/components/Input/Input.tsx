import * as React from 'react'

const Input: React.SFC<any> = (props) => {
  const { rewizard, label, onChange } = props
  const { state } = rewizard
  const { valid, value, msg } = state

  const style = {
    padding: '.5rem 1rem',
    borderRadius: '5px',
    border: `solid 1px ${valid === false ? '#FF0000' : '#b4b4b4'}`,
    marginRight: '1rem'
  }

  return (
    <div style={{ marginBottom: '1rem' }}>
      <label style={{ display: 'block', fontFamily: 'sans-serif' }}>{label}</label>
      <input style={style} value={value || ''} onChange={(e) => onChange(e.target.value)} />
      <span>{msg}</span>
    </div>
  )
}

export default Input
