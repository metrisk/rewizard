// Components
import Step from '../components/Step/Step'

/**
 * A sample set of steps to get you up and running 😇
 */
const steps = [
  {
    entryPoint: true, // There must be one entryPoint, so the Wizard knows where to start from
    id: 'one', // Required
    component: Step, // Required
    props: {
      myTestProp: 'test' // These props will be passed as normal to your component
    },
    fieldIds: ['name', 'last-name', 'hidden'], // Required
    next: 'two' // Only required if not last step
  },
  {
    id: 'two',
    component: Step,
    fieldIds: ['animal', 'size', 'colour'],
    next: [
      { fieldId: 'animal', value: 'cat', nextId: 'two-cat' },
      { fieldId: 'animal', value: 'horse', nextId: 'two-horse' },
      { fieldId: 'animal', value: /^(?!cat|horse).*/, skipTo: 'three' }
    ]
  },
  {
    id: 'two-cat',
    component: Step,
    fieldIds: ['cat-details'],
    next: 'three'
  },
  {
    id: 'two-horse',
    component: Step,
    fieldIds: ['horse-details'],
    next: 'three'
  },
  {
    id: 'three',
    component: Step,
    fieldIds: ['temperament', 'cost', 'notes']
  }
]

export default steps
