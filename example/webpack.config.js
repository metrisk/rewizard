const path = require('path');

// PLugins
const Prettier = require('prettier-webpack-plugin');

const config = (options) => {
  return {
    mode: options.env,
    devtool: 'source-map',
    entry: options.entry,
    output: {
      path: path.resolve(__dirname, 'dist'),
      publicPath: '/',
      filename: 'index.js',
    },
    resolve: {
      extensions: ['.ts', '.tsx', '.js', '.json'],
      modules: ['node_modules']
    },
    plugins: [
      new Prettier({
        printWidth: 120,
        tabWidth: 2,
        useTabs: false,
        semi: false,
        singleQuote: true,
        arrowParens: 'always',
        encoding: 'utf-8',
        extensions: ['.js', '.ts', '.tsx', '.jsx', '.json']
      })
    ],
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          loader: 'ts-loader',
          exclude: ['/node_modules']
        },
      ]
    }
  };
}

module.exports = config;
