import * as React from 'react'
import * as ReactSixteenAdapter from 'enzyme-adapter-react-16'
import { configure } from 'enzyme'
import { shallow } from 'enzyme'
import { expect } from 'chai'

configure({ adapter: new ReactSixteenAdapter() })

// Test the wizard components render
const Field: React.SFC<any> = (props) => {
  const { rewizard, onChange } = props
  return (
    <input onChange={(e) => onChange(e.target.value)} />
  )
}
describe("<Field>", () => {
  it('Renders without crashing', () => {
    shallow(<Field onChange={(e: any) => e}/>)
  })
})