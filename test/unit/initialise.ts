import 'mocha'
import { expect } from 'chai'
import { init } from '../../src/utils/initialise'

describe('----- Init Tests -----', () => {

    it('Should allow field to be optional based on another fields value', () => {
        const props = {
          fields: [
            {
              id: 'firstname',
              optional: (fields: any) => fields['lastname'].value === 'test',
            },
            {
              id: 'lastname'
            },
          ]
        }
        const fieldConfig: any = {
          id: 'firstname',
        }
        const prev: any = {
          fields: { 
            firstname: { 
              value: '',
              msg: ''
            },
            lastname: { 
              value: 'test'
            },
          },
        }
    
        const expected = {
          fields: { 
            firstname: { 
              value: '',
              valid: true,
              msg: '',
            },
            lastname: {
              value: 'test',
            }
          },
          steps: {}
        }
    
        expect(init(prev, props)).to.deep.equal(expected)
      })
    
    })