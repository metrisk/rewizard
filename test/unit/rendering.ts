import 'mocha'
import { expect } from 'chai'
import { getNext, getPreReq, getPreReqOptions } from '../../src/utils/rendering'

describe('----- Field State Object Mutation Tests -----', () => {

  it('Should add prerequired field to field object', () => {
    const fields: any = {
      'field-one': {
        value: 'apple 09/45 -!2m',
      }
    }
    const config: any = {
      id: 'field-two',
      prereq: {
        fieldId: 'field-one',
        value: '09\/45'
      }
    }
    const expected: any = {
      fields: {
        'field-one': {
          value: 'apple 09/45 -!2m',
        },
        'field-two': {}
      }
    }
    expect(getPreReq(fields, config)).to.deep.equal(expected)
  })

  it('Should not add prerequired field to object', () => {
    const fields: any = {
      'field-one': {
        value: 'apple',
      },
      'field-two': {
        value: '',
      }
    }
    const config: any = {
      id: 'field-two',
      prereq: {
        fieldId: 'field-one',
        value: 'banana'
      }
    }
    const expected: any = {
      fields: {
        'field-one': {
          value: 'apple',
        }
      }
    }
    expect(getPreReq(fields, config)).to.deep.equal(expected)
  })

  it('Should add prerequired options to object', () => {
    const fields: any = {
      'field-one': {
        value: 'red',
      },
      'field-two': {}
    }
    const config: any = {
      id: 'field-two',
      options: [
        { value: 'apple', label: 'Apple', prereq: { fieldId: 'field-one', value: 'red' }},
        { value: 'banana', label: 'Banana'}
      ]
    }
    const expected: any = {
      fields: {
        'field-one': {
          value: 'red',
        },
        'field-two': {
          options: [
            { value: 'banana', label: 'Banana'},
            { value: 'apple', label: 'Apple', prereq: { fieldId: 'field-one', value: 'red' }}
          ],
        }
      }
    }
    expect(getPreReqOptions(fields, config)).to.deep.equal(expected)
  })

})

describe('----- Step State Object Mutation Tests -----', () => {

  it('Should add the correct nextId (basic)', () => {
    const steps: any = {
      'step-one': {}
    }
    const step: any = {
      id: 'step-one',
      next: 'step-two'
    }
    const expected: any = {
      steps: {
        'step-one': {
          next: 'step-two'
        }
      }
    }
    expect(getNext(steps, {}, step, {})).to.deep.equal(expected)
  })

  it('Should add the correct nextId (conditional)', () => {
    const steps: any = {
      'step-one': {}
    }
    const fields: any = {
      'field-one': {
        value: 'apple'
      }
    }
    const step: any = {
      id: 'step-one',
      next: [
        { fieldId: 'field-one', value: 'apple', nextId: 'fruits' },
        { fieldId: 'field-one', value: 'steak', nextId: 'meat' },
      ]
    }
    const field: any = {
      id: 'field-one'
    }
    const expected: any = {
      steps: {
        'step-one': {
          next: 'fruits'
        }
      }
    }
    expect(getNext(steps, fields, step, field)).to.deep.equal(expected)
  })

  it('Should add the correct nextId (skipTo)', () => {
    const steps: any = {
      'step-one': {}
    }
    const fields: any = {
      'field-one': {
        value: 'steak'
      }
    }
    const step: any = {
      id: 'step-one',
      next: [
        { fieldId: 'field-one', value: 'apple', nextId: 'fruits' },
        { fieldId: 'field-one', value: 'steak', skipTo: 'meat' },
      ]
    }
    const field: any = {
      id: 'field-one'
    }
    const expected: any = {
      steps: {
        'step-one': {
          next: 'meat'
        }
      }
    }
    expect(getNext(steps, fields, step, field)).to.deep.equal(expected)
  })

})
