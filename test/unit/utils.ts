import 'mocha'
import * as Chance from 'chance'
import { expect } from 'chai'
import { uniq, makeArr, isInvalidated, objHasVal, uniqKeys, uniqVals, objHasKey } from '../../src/utils/helpers'
const chance = new Chance()

describe('----- Utils Tests -----', () => {

  it('Function: uniq() - Should only add values to array if they don\'t exist already', () => {
    const init = ['test1', 'test1', 'test1', 'test2', 'test2', 'test3'].reduce((acc: any, x: any) => {
      uniq(acc, x)
      return acc
    }, [])
    const expected: any = ['test1', 'test2', 'test3']
    expect(init).to.deep.equal(expected)
  })

  it('Function: makeArr() - Should convert a string to array and leave array alone', () => {
    const initString = 'test1'
    const initArr = 'test1'
    const expected: any = ['test1']

    expect(makeArr(initString)).to.deep.equal(expected)
    expect(makeArr(initArr)).to.deep.equal(expected)
  })

  it('Function: isInvalidated() - Check if an object item is invalidated', () => {
    const initObj = {
      test1: {
        invalidate: [ 'test2' ]
      },
      test2: {
        invalidate: [ 'test3', 'test4' ]
      }
    }
    const initObjTrue = {
      test1: {
        invalidate: [ 'test1' ]
      },
      test2: {
        invalidate: [ 'test3', 'test4' ]
      }
    }
    
    expect(isInvalidated(initObj, 'test1')).to.equal(false)
    expect(isInvalidated(initObjTrue, 'test1')).to.equal(true)
  })

  it('Function: uniqVals() - Should check for duplicate values, else throw error', () => {
    const key = 'id'
    const uniq: any = [
      {
        id: chance.word()
      },
      {
        id: chance.word()
      }
    ]
    const same = chance.word()
    const notUniq: any = [
      {
        id: same
      },
      {
        id: same
      }
    ]

    expect(uniqVals(uniq, key)).to.equal(false)
    expect(() => uniqVals(notUniq, key)).to.throw()
  })
  
  it('Function: uniqKeys() - Should check for duplicate keys, else throw error', () => {
    const key = 'entryPoint'
    const singleEntry: any = [
      {
        entryPoint: true
      }
    ]
    const dupEntry: any = [
      {
        entryPoint: true
      },
      {
        entryPoint: true
      }
    ]

    expect(uniqKeys(singleEntry, key)).to.equal(false)
    expect(() => uniqKeys(dupEntry, key)).to.throw()
  })

  it('Function: objHasVal() - Should check key existence, else throw error', () => {
    const fieldOne = chance.word()
    const fieldTwo = chance.word()
    const key = 'id'
    const keyValues = [fieldOne,fieldTwo]
    const keyValuesErr = [chance.word(), fieldTwo]
    const fields: any = [
      {
        id: fieldOne
      },
      {
        id: fieldTwo
      },
    ]

    expect(objHasVal(fields, keyValues, key)).to.equal(true)
    expect(() => objHasVal(fields, keyValuesErr, key)).to.throw()
  })

  it('Function: objHasKey() - Should check for at least one of each key, else throw error', () => {
    const keys = ['entryPoint', 'submit']
    const keysErr = ['entryPoint', 'test']
    const steps: any = [
      {
        entryPoint: true
      },
      {
        submit: true
      }
    ]
  
    expect(objHasKey(steps, keys)).to.equal(true)
    expect(() => objHasKey(steps, keysErr)).to.throw()
  })
})