import 'mocha'
import { expect } from 'chai'
import { validateField } from '../../src/utils/validation'

describe('----- Field Object Mutation Tests -----', () => {
  const init: any = {
    one: { 
      valid: false
    }
  }

  const expectedTrue: any = {
    fields: { 
      one: { 
        msg: '',
        valid: true
      }
    }
  }

  const expectedFalse: any = {
    fields: { 
      one: { 
        msg: '',
        valid: false,
      }
    }
  }
  
  const validation: any = {
    fn: (value: any) => value ? true : false
  }

  it('Should validate the field using inbuilt validation and return true', () => {
    expect(validateField(init, 'one', undefined, 'value')).to.deep.equal(expectedTrue)
  })
  
  it('Should validate the field using inbuilt validation and return false', () => {
    expect(validateField(init, 'one', undefined, 'null')).to.deep.equal(expectedFalse)
  })
  
  it('Should validate the field using inbuilt validation and return false', () => {
    expect(validateField(init, 'one', undefined, ' ')).to.deep.equal(expectedFalse)
  })
  
  it('Should validate the field using inbuilt validation and return false', () => {
    expect(validateField(init, 'one', undefined, 'false')).to.deep.equal(expectedFalse)
  })
  
  it('Should validate the field using inbuilt validation and return false', () => {
    expect(validateField(init, 'one', undefined, false)).to.deep.equal(expectedFalse)
  })
  
  it('Should validate the field using custom function and return true', () => {
    expect(validateField(init, 'one', validation, 'value')).to.deep.equal(expectedTrue)
  })
  
  it('Should validate the field using custom function and return false', () => {
    expect(validateField(init, 'one', validation, '')).to.deep.equal(expectedFalse)
  })
  
})
 