import 'mocha'
import { expect } from 'chai'
import { update, updateState } from '../../src/utils/actions'

describe('----- Field Object Mutation Tests -----', () => {

  it('Should update the obj with value', () => {
    const init: any = {
      one: { 
        value: ''
      }
    }
    const expected: any = {
      fields: { 
        one: { 
          value: 'value'
        }
      }
    }
    expect(update(init, 'one', 'value')).to.deep.equal(expected)
  })

  it('Should allow field to be optional based on another fields value', () => {
    const props = {
      fields: [
        {
          id: 'firstname',
          optional: (fields: any) => fields['lastname'].value === 'test',
        },
        {
          id: 'lastname'
        },
      ]
    }
    const fieldConfig: any = {
      id: 'firstname',
    }
    const prev: any = {
      fields: { 
        firstname: { 
          value: '',
          msg: ''
        },
        lastname: { 
          value: 'test'
        },
      },
    }

    const expected = {
      fields: { 
        firstname: { 
          value: '',
          valid: true,
          msg: '',
        },
        lastname: {
          value: 'test',
        }
      },
      steps: {}
    }

    expect(updateState(fieldConfig, '')(prev, props)).to.deep.equal(expected)
  })

})
