const path = require('path');
const root = path.resolve(__dirname);

// Plugins
const Prettier = require('prettier-webpack-plugin');

const config = {
  mode: 'production',
  entry: './src/index.ts',
  output: {
    path: path.resolve(root, 'dist'),
    publicPath: '/',
    filename: 'index.js',
    library: '',
    libraryTarget: 'commonjs'
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.json'],
    alias: {
      '@app': path.resolve(root, 'src/'),
      '@utils': path.resolve(root, 'src/utils/'),
      '@components': path.resolve(root, 'src/components/'),
      '@hoc': path.resolve(root, 'src/hoc/'),
    },
    modules: ['node_modules']
  },
  plugins: [
    new Prettier({
      printWidth: 120,
      tabWidth: 2,
      useTabs: false,
      semi: false,
      singleQuote: true,
      arrowParens: 'always',
      encoding: 'utf-8',
      extensions: ['.js', '.ts', '.tsx', '.jsx', '.json']
    })
  ],
  module: {
    rules: [
      {
        test: /\.(tsx|ts)$/,
        exclude: /node_modules/,
        loader: 'ts-loader'
      },
    ]
  }
}

module.exports = config;
