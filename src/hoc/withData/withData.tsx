import * as React from 'react'
import { IProps } from './types'

const withData = (Component: any) =>
  class WithData extends React.PureComponent<IProps> {
    attachData = () => {
      const { data, fields } = this.props
      return fields.reduce((acc: any, x: any) => [...acc, { ...x, savedValue: data[x.id] && data[x.id] }], [])
    }

    render() {
      const { fields, ...other } = this.props
      const dataFields = this.attachData()

      return <Component fields={dataFields} {...other} />
    }
  }

export default withData
