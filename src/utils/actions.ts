import { makeArr, isInvalidated } from './helpers'
import { getNext, getPrev, getPreReq, getPreReqOptions } from './rendering'
import { validateStep, validateField } from './validation'

/**
 * Updates the wizard field state with the current value
 *
 * @param {Object} fields
 * The current field state for all fields
 *
 * @param {String} id
 * The id of the current field
 *
 * @param {String | Array | Number | Boolean} value
 * The current field value
 */
const update = (fields: any, id: any, value: any) => {
  if (!fields[id]) return
  fields[id].value = value
  return {
    fields: fields
  }
}

/**
 * Show or hide a field or step
 *
 * @param {Object} obj
 * The previous wizard state
 *
 * @param {String} type
 * The type - 'fields' or 'steps'
 *
 * @param {String} id
 * The current field or step ID
 *
 */
const show = (obj: any, type: string, id: string) => {
  !obj[id] && (obj[id] = {})
  return {
    [type]: obj
  }
}

/**
 * Show or hide the next step in the tree
 *
 * @param {Object} step
 * The current step object
 */
const showNext = (steps: any, step: any): any => {
  if (!steps[step.id]) return
  if (!steps[step.id].next) return
  if (steps[step.id].valid) {
    return show(steps, 'steps', steps[step.id].next)
  }
}

/**
 * Remove the step or field from the state object
 *
 * @param {Object} obj
 * The field or step state object
 *
 * @param {Array} ids
 * The ids (keys) to remove
 */
const deleteFromObj = (obj: any, id: string | string[]) => {
  const ids = makeArr(id)
  obj = ids.map((x: any) => delete obj[x])
}

/**
 * Set a field or step to valid
 *
 * @param {Object} obj
 * The previous wizard state
 *
 * @param {String} type
 * The type - 'fields' or 'steps'
 *
 * @param {String} id
 * The current field or step ID
 *
 */
const setValid = (obj: any, type: string, id: string) => {
  if (!obj[id]) return
  obj[id].valid = true
  return {
    [type]: obj
  }
}

/**
 * Set a field or step to hidden
 *
 * @param {Object} obj
 * The previous wizard state
 *
 * @param {String} type
 * The type - 'fields' or 'steps'
 *
 * @param {String} id
 * The current field or step ID
 *
 */
const setHidden = (obj: any, type: string, id: string) => {
  if (!obj[id]) return
  obj[id].hidden = true
  return {
    [type]: obj
  }
}

/**
 * Add invalidated steps to the state object based off the
 * current input's value
 *
 * @param {Object} steps
 * The current state of all steps
 *
 * @param {Object} fields
 * The current state of all fields
 *
 * @param {Object} step
 * The current step config
 *
 * @param {Object} field
 * The current field config
 */
const invalidateSteps = (steps: any, fields: any, step: any, field: any) => {
  const { value } = fields[field.id]
  if (!value) return
  if (!steps[step.id]) return

  if (field.options && Array.isArray(step.next)) {
    const option = step.next.filter((x: any) => x.value !== value)
    const notNext = option.filter((x: any) => x.nextId !== steps[step.id].next)
    steps[step.id].invalidate = notNext.map((x: any) => x.nextId)
    return {
      steps: steps
    }
  }
}

/**
 * Add invalidated fields to the state object based off the
 * current input's value
 *
 * @param {Object} fields
 * The current state of all fields
 *
 * @param {Object} field
 * The current field config
 */
const invalidateFields = (fields: any, field: any) => {
  const { value } = fields[field.id]
  let inval = ''
  if (value && field.options) {
    const option = field.options.find((x: any) => x.value === value)
    inval = option && option.invalidate ? option.invalidate : null
    fields[field.id].invalidate = fields[field.id].valid ? inval : null
  } else if (value && field.invalidate) {
    inval = field.invalidate
    fields[field.id].invalidate = fields[field.id].valid ? inval : null
  }
  return {
    fields: fields
  }
}

/**
 * Loop through the wizard state tree and apply all functions (called on input change)
 *
 * @param {Object} config
 * The current field config
 *
 * @param {String || Array || Boolean} value
 * The current field value
 */
const updateState = (config: any, value: any) => (prev: any, props: any) => {
  const steps = { ...prev.steps }
  const fields = { ...prev.fields }
  const { id, validation } = config

  update(fields, id, value)
  validateField(fields, id, validation, value)

  props.fields.forEach((f: any) => {
    // This ensures a compareWith field has two way checks
    if (f.validation && f.validation.compareWith === id) {
      validateField(fields, f.id, f.validation, fields[f.id].value)
    }

    if (!isInvalidated(fields, f.id)) {
      show(fields, 'fields', f.id)
      invalidateFields(fields, f)
      if (props.steps) {
        const step = props.steps.find((x: any) => x.fieldIds && x.fieldIds.includes(f.id))
        getNext(steps, fields, step, f)
        invalidateSteps(steps, fields, step, f)
      }
      getPreReq(fields, f)
      getPreReqOptions(fields, f)

      const optional = typeof f.optional === 'function' ? f.optional(fields) : f.optional
      if (optional) {
        setValid(fields, 'fields', f.id)
      }
    } else {
      deleteFromObj(fields, [f.id])
    }
  })

  if (props.steps) {
    props.steps.forEach((s: any) => {
      if (isInvalidated(steps, s.id)) {
        deleteFromObj(steps, [s.id])
        deleteFromObj(fields, s.fieldIds)
      } else {
        validateStep(steps, fields, s)
        showNext(steps, s)
        getPrev(steps, s)
      }
    })
  }

  return {
    steps: steps,
    fields: fields
  }
}

export { update, show, showNext, deleteFromObj, setValid, setHidden, invalidateSteps, invalidateFields, updateState }
