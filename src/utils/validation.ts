/**
 * Check to see that all fields within the current step are valid
 * and set the step state.valid
 *
 * @param {Object} step
 * The current step object
 */
const validateStep = (steps: any, fields: any, step: any) => {
  if (!steps[step.id]) return
  const thisFields = Object.keys(fields).reduce((acc: any, x: any) => {
    step.fieldIds.includes(x) && acc.push({ ...fields[x] })
    return acc
  }, [])
  steps[step.id].valid = thisFields.every((x: any) => x.valid === true)
  return {
    steps: steps
  }
}

/**
 * Validate the current field using the passed in validation function
 *
 * @param {Object} fields
 * The current field state for all fields
 *
 * @param {String} id
 * The id of the current field
 *
 * @param {Function} validation
 * The passed in validation function
 *
 * @param {String || Array || Boolean} value
 * The current field value
 */
const validateField = (fields: any, id: any, validation: any, value: any) => {
  if (!fields[id]) return
  let valid = false
  const {
    fn = (value: any) => new RegExp(/^(?!\s*$|false|null).*/, 'g').test(value),
    compareWith = null,
    successMsg = '',
    errorMsg = ''
  } = validation || {}

  if (compareWith) {
    const otherField = fields[compareWith] || {}
    valid = fn(value, otherField.value || '')
  } else {
    valid = fn(value)
  }

  fields[id].valid = valid
  fields[id].msg = valid ? successMsg : errorMsg

  return {
    fields
  }
}

/**
 * Return a new object with all invalid fields
 *
 * @param {Object} fields
 * The wizard field state
 */
const findInvalid = (fields: any) =>
  Object.keys(fields).reduce((acc: any, x: any) => (!fields[x].valid ? { ...acc, [x]: fields[x] } : acc), {})

/**
 * Check to see the validity of all fields in the entire wizard -
 * i.e the final validation check before sending data
 */
const validateAll = (fields: any) => Object.keys(fields).every((x: any) => fields[x] && fields[x].valid)

export { validateStep, validateField, findInvalid, validateAll }
