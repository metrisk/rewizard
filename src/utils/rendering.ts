import { show, deleteFromObj } from './actions'
import { uniq } from './helpers'

/**
 * Get the previous step ID
 *
 * @param {Object} steps
 * The current state of all steps
 *
 * @param {Object} step
 * The current step config
 */
const getPrev = (steps: any, step: any) => {
  if (!step) return
  if (!steps[step.id]) return
  const prev = Object.keys(steps).find((x: any) => steps[x].next === step.id)
  steps[step.id].prev = prev

  return {
    steps: steps
  }
}

/**
 * Get the next step ID from the current input value if it's
 * condition, else just set the ID as the step's next value
 *
 * @param {Object} steps
 * The current state of all steps
 *
 * @param {Object} fields
 * The current state of all fields
 *
 * @param {Object} step
 * The current step config
 *
 * @param {Object} field
 * The current field config
 */
const getNext = (steps: any, fields: any, step: any, field: any) => {
  if (!step) return
  if (!steps[step.id]) return
  if (Array.isArray(step.next)) {
    if (!fields[field.id]) return
    const { value } = fields[field.id]
    const option = step.next.find((x: any) => x.fieldId === field.id && new RegExp(x.value, 'g').test(value))
    if (!option) return
    option.nextId && (steps[step.id].next = option.nextId)
    option.skipTo && (steps[step.id].next = option.skipTo)
  } else {
    steps[step.id].next = step.next
  }
  return {
    steps: steps
  }
}

/**
 * Get input pre-requirements and only show the field if conditions met
 *
 * @param {Array} fields
 * The field prop array with attached data
 *
 * @param {String} value
 * The value of the current input
 */
const getPreReq = (fields: any, config: any) => {
  const { id, prereq } = config
  if (!prereq) return
  if (typeof prereq === 'string') {
    if (!fields[prereq]) return
    fields[prereq].valid ? show(fields, 'fields', id) : deleteFromObj(fields, [id])
  } else {
    if (!fields[prereq.fieldId]) return
    new RegExp(prereq.value, 'g').test(fields[prereq.fieldId].value)
      ? show(fields, 'fields', id)
      : deleteFromObj(fields, id)
  }
  return {
    fields: fields
  }
}

/**
 * Get input pre-requirements, for example, options within a select that
 * populate another select will have pre-requirements
 *
 * @param {Array} fields
 * The field prop array with attached data
 *
 * @param {String} value
 * The value of the current input
 */
const getPreReqOptions = (fields: any, config: any) => {
  const { id, options } = config
  let opts: any = []
  if (!options) return
  options.forEach((x: any) => {
    if (!x.prereq || !fields[x.prereq.fieldId]) return
    new RegExp(x.prereq.value, 'g').test(fields[x.prereq.fieldId].value) && uniq(opts, x)
  })
  fields[id] && (fields[id].options = [...options.filter((x: any) => !x.prereq), ...opts])
  return {
    fields: fields
  }
}

export { getPrev, getNext, getPreReq, getPreReqOptions }
