/**
 * Unique add a string item to array
 *
 * @param {Array} arr
 * The array to add to
 *
 * @param {String} value
 * The item to add
 */
const uniq = (arr: any[], value: string) => arr.indexOf(value) === -1 && arr.push(value)

/**
 * Make a string into an array if it isn't already
 *
 * @param {String | Array} val
 * The value to return or add to array
 */
const makeArr = (val: string | string[]) => (Array.isArray(val) ? val : [val.toString()])

/**
 * Check to see if the current step or field has been invalidated
 * by a previous field, and hide/show as necessary
 *
 * @param {String} obj
 * The type - 'fields' or 'steps'
 *
 * @param {String} id
 * The current field or step ID
 */
const isInvalidated = (obj: any, id: any): any => {
  const match = (x: any) => obj[x].invalidate && obj[x].invalidate.includes(id)
  const found = Object.keys(obj).find(match)
  return found ? true : false
}

/**
 * Check an array of objects for duplicate values (shallow)
 *
 * @param {Array} arr
 * The array of objects to check values
 *
 * @param {String} value
 * The value which should be unique
 */
const uniqVals = (arr: any, value: any) => {
  const keys = arr.map((x: any) => x[value])
  const isDup = keys.some((x: any, y: any) => keys.indexOf(x) != y)
  if (isDup) throw `These values must be unique: ${keys}`
  return isDup
}

/**
 * Check an array of objects for duplicate keys (shallow)
 *
 * @param {Array} arr
 * The array of objects to check keys
 *
 * @param {String} key
 * The key which should be unique
 */
const uniqKeys = (arr: any, key: string) => {
  const keys = arr.map((x: any) => (x[key] ? key : null))
  const isDup = keys.some((x: any, y: any) => x && keys.indexOf(x) !== y)
  if (isDup) throw `There are duplicate keys which must be unique: ${keys}`
  return isDup
}

/**
 * Takes an array of objects and checks that the key provided contains all the values
 *
 * @param {Array} srch
 * The array of objects to search in
 *
 * @param {Array} vals
 * The source array of strings
 *
 * @param {Array} key
 * The key to check the value of
 */
const objHasVal = (srch: any, vals: any, key: string) => {
  const allVals = srch.map((x: any) => x[key])
  const yes = vals.every((x: any) => allVals.includes(x))
  if (!yes) {
    const no = srch.map((x: any) => (!vals.includes(x[key]) ? x[key] : null))
    throw `The fields: '${no}' cannot be found`
  }
  return yes
}

/**
 * Takes an array of strings and checks another array for an object with that key
 *
 * @param {Array} src
 * The source array of strings
 *
 * @param {Array} srch
 * The array of objects to search in
 */
const objHasKey = (srch: any, src: any) => {
  const keys = srch.map((x: any) => Object.keys(x)).flat()
  const yes = src.every((x: any) => keys.includes(x))
  if (!yes) {
    const no = src.map((x: any) => (!keys.includes(x) ? x : null))
    throw `The fields: '${no}' cannot be found`
  }
  return yes
}

/**
 * Retrieve only fields which are hidden
 *
 * @param {Object} fields
 * The wizard field state
 */
const findHidden = (fields: any) =>
  Object.keys(fields).reduce((acc: any, x: any) => (fields[x].hidden ? { ...acc, [x]: fields[x] } : acc), {})

/**
 * Return a new object, excluding certain keys
 *
 * @param {Array} exclude
 * An array of field ids to exclude
 *
 * @param {Object} fields
 * The wizard field state
 */
const exclude = (exclude: any, fields: any) =>
  Object.keys(fields).reduce((acc: any, x: any) => (!exclude.includes(x) ? { ...acc, [x]: fields[x] } : acc), {})

/**
 * Retrieve only the values from the object
 *
 * @param {Object} fields
 * The wizard field state
 */
const format = (fields: any) => {
  const obj: any = {}
  Object.keys(fields).map((x: any) => {
    obj[x] = typeof fields[x].value !== 'undefined' ? fields[x].value : null
  })
  return obj
}

export { uniq, makeArr, isInvalidated, uniqVals, uniqKeys, objHasVal, objHasKey, findHidden, exclude, format }
