import { uniqVals, uniqKeys, objHasKey, objHasVal } from './helpers'
import { show, setValid, setHidden, update } from './actions'
import { getPreReq, getPreReqOptions } from './rendering'

/**
 * Check that the config data passed in is valid
 *
 * @param {Array} steps
 * The steps config array
 *
 * @param {Array} fields
 * The fields config array
 */
const checkAll = (steps: any, fields: any) => {
  if (!fields) return false
  try {
    if (steps) {
      objHasKey(steps, ['entryPoint'])
      uniqVals(steps, 'id')
      uniqKeys(steps, 'entryPoint')
      steps.forEach((x: any) => x.fieldIds && objHasVal(fields, x.fieldIds, 'id'))
    }
    uniqVals(fields, 'id')
    return true
  } catch (e) {
    throw e
  }
}

/**
 * Build the initial state tree with steps & field objects
 *
 * @param {Object} prev
 * The previous state (in this case will be initial state)
 *
 * @param {Array} props
 * The wizard props
 */
const init = (prev: any, props: any) => {
  const steps = { ...prev.steps }
  const fields = { ...prev.fields }

  if (props.steps) {
    props.steps.forEach((s: any) => {
      s.entryPoint &&
        (steps[s.id] = {
          valid: false
        })
      s.fieldIds && s.fieldIds.forEach((f: any) => show(fields, 'fields', f))
    })
  }

  props.fields.forEach((f: any) => {
    if (!props.steps) {
      show(fields, 'fields', f.id)
    }

    getPreReq(fields, f)
    getPreReqOptions(fields, f)

    const optional = typeof f.optional === 'function' ? f.optional(fields) : f.optional
    if (optional) {
      setValid(fields, 'fields', f.id)
    }

    if (f.hidden) {
      setValid(fields, 'fields', f.id)
      setHidden(fields, 'fields', f.id)
      update(fields, f.id, f.value)
    }
  })

  return {
    steps: steps,
    fields: fields
  }
}

export { checkAll, init }
