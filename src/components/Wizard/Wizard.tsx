import * as React from 'react'
import { IProps } from './types'
import Form from '@components/Form'

// Initialise Wizard
import { checkAll } from '@utils/initialise'

// HOC
import withData from '@hoc/withData/withData'
const WithData = withData(Form)

const Wizard = (props: IProps) => {
  const {
    config = {
      nested: false,
      savedData: null,
      onSubmit: (data: any) => alert(data)
    },
    steps = null,
    fields = null
  } = props

  if (checkAll(steps, fields)) {
    return config.savedData ? (
      <WithData key={'rewizard-withdata'} data={config.savedData} {...props} />
    ) : (
      <Form key={'rewizard-form'} {...props} />
    )
  } else {
    return null
  }
}

export default Wizard
