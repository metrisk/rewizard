import { ReactNode } from 'react'

interface IProps {
  config: IConfig
  steps: IStepConfig[]
  fields: IFieldConfig[]
  children?: any
}

interface IConfig {
  nested?: boolean
  savedData?: any
  onSubmit: (data: any, params?: any) => any
}

interface IStepConfig {
  id: string
  component: ReactNode
  props?: any
  fieldIds: string[]
  entryPoint?: boolean
  save?: boolean
  submit?: boolean
}

type OptionalFunction = (fields: any) => boolean

interface IFieldConfig {
  id: string
  component: ReactNode
  props?: any
  readOnly?: boolean
  validation?: any
  savedValue?: any
  prereq?: any
  invalidate?: any
  options?: any[]
  data?: any
  optional?: boolean | OptionalFunction
  hidden?: boolean
}

export { IProps }
