interface IProps {
  config: any
  state: any
  children?: any
  onCancel?: () => void
  onSave?: () => void
  onSubmit?: () => void
}

export { IProps }
