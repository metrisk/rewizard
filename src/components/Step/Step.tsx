import * as React from 'react'
import { IProps } from './types'

const Step: React.SFC<IProps> = (props) => {
  const { config, state, children, onSubmit } = props
  const Component: any = config.component

  return (
    <Component
      rewizard={{
        id: config.id,
        state: state,
        onSubmit: onSubmit
      }}
      {...config.props}
    >
      {children}
    </Component>
  )
}

export default Step
