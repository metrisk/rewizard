import * as React from 'react'
import { IProps, IState } from './types'

// Build functions
import { init } from '@utils/initialise'
import { updateState } from '@utils/actions'
import { findHidden, exclude, format, makeArr } from '@utils/helpers'
import { findInvalid, validateAll } from '@utils/validation'

// Component
import Step from '@components/Step/Step'
import Field from '@components/Field/Field'

class Form extends React.PureComponent<IProps> {
  state: IState = {
    steps: {},
    fields: {}
  }

  componentWillMount = () => {
    this.setState(init)
  }

  getState = (id?: string | string[]) => {
    const { fields } = this.state
    const obj = id
      ? Object.keys(fields).reduce(
          (acc: any, x: any) => (makeArr(id).includes(x) ? { ...acc, [x]: fields[x] } : acc),
          {}
        )
      : fields
    const hidden: object = findHidden(obj)
    const invalid: object = findInvalid(obj)
    const allValid: boolean = validateAll(obj)
    const finalData = exclude([...Object.keys(hidden), ...Object.keys(invalid)], obj)

    return {
      valid: allValid,
      hidden: format(hidden),
      invalid: format(invalid),
      data: format(finalData)
    }
  }

  getValue = (id: any) => {
    const { fields: fieldState } = this.state
    if (!fieldState[id]) return

    return fieldState[id].value
  }

  handleChange = (config: any, value: any) => {
    this.setState(updateState(config, value))
  }

  handleClick = (params?: any) => {
    const { config } = this.props
    const obj = this.getState()

    return config.onSubmit(obj, ...params)
  }

  handleSubmit = (e: React.FormEvent) => {
    e.preventDefault()
    const { config } = this.props
    const data = this.getState()

    return config.onSubmit(data)
  }

  renderSteps = () => {
    const { steps } = this.props
    const { steps: stepState } = this.state

    return steps.map((x: any) =>
      stepState[x.id] ? (
        <Step key={`step-${x.id}`} config={x} state={stepState[x.id]} onSubmit={this.handleClick}>
          {this.renderFields(x.fieldIds)}
        </Step>
      ) : null
    )
  }

  renderFields = (fieldIds: string[]) => {
    const { fields } = this.props
    const { fields: fieldState } = this.state

    return fields.map((y: any) =>
      fieldIds.includes(y.id) && !y.hidden && fieldState[y.id] ? (
        <Field
          key={`field-${y.id}`}
          config={y}
          state={fieldState[y.id]}
          getValue={this.getValue}
          getState={this.getState}
          onChange={this.handleChange}
        />
      ) : null
    )
  }

  render() {
    const { config, fields, steps, children, ...other } = this.props
    const { fields: stateFields } = this.state

    const Tag: any = config.nested ? React.Fragment : 'form'

    return (
      <Tag {...(config.nested ? other : { ...other, onSubmit: this.handleSubmit })}>
        {children
          ? children({
              rewizard: {
                valid: validateAll(stateFields),
                children: steps ? this.renderSteps() : this.renderFields(fields.map((x: any) => x.id)),
                onSubmit: this.handleClick
              },
              ...other
            })
          : this.renderSteps()}
      </Tag>
    )
  }
}

export default Form
