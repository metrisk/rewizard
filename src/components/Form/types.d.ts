interface IProps {
  config: any
  steps: any
  fields: any
  children?: any
}

interface IState {
  steps: any
  fields: any
}

export { IProps, IState }
