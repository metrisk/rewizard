interface IProps {
  config: any
  state: any
  getValue: (id?: string | string[]) => any
  getState: (id?: string | string[]) => any
  onChange: (config: any, value: any) => void
}

export { IProps }
