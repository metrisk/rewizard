import * as React from 'react'
import { IProps } from './types'

class Field extends React.Component<IProps> {
  componentDidMount = () => {
    this.setInitialValue()
  }

  setInitialValue = () => {
    const { savedValue } = this.props.config
    if (savedValue === null || typeof savedValue === 'undefined') return
    this.handleChange(savedValue)
  }

  handleChange = (value: any) => {
    const { config, onChange } = this.props
    onChange(config, value)
  }

  render() {
    const { config, state, getValue, getState } = this.props
    const Component: any = config.component

    return (
      <Component
        rewizard={{
          id: config.id,
          state: state,
          savedValue: config.savedValue,
          disabled: config.readOnly,
          optional: config.optional,
          getValue: getValue,
          getState: getState,
          onChange: this.handleChange
        }}
        {...config.props}
      />
    )
  }
}

export default Field
