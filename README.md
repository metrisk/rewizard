# Wizard  🧙‍♂️

A React multi-step form/wizard which handles tree-structured routes, conditional step/field rendering and validation. To use:

```sh
$ npm install rewizard
```

Import Wizard component into your project:
```js
import Wizard from 'rewizard'

const config = {
  // Explained below
}

const steps = [
  // Explained below
]

const fields = [
  // Explained below
]

export const App = () => <Wizard config={config} steps={steps} fields={fields} />
ReactDOM.render(<App />, document.getElementById('app'))
```

## Usage

### Config
Define your wizard configuration options(optional):

```js
const config = {
  showNav: true,
  savedData: data,
  prevStep: true,
  nextStep: true,
  onSave: (e: any) => console.log(e),
  onSubmit: (e: any) => console.log(e)
}
```

| Option | Required | Type | Description | Default |
|:-|:-|:-|:-|:-|
| showNav | `false` | *Boolean* | Shows a mini, dot-style navigation | `true` |
| savedData | `false` | *Object*: `{[key: string]: value}` | Insert pre-existing data into the wizard. | `null` |
| prevStep | `false` | *Boolean* | Show a next button | `true |
| nextStep | `false` | *Boolean* | Show a prev button | `true` |
| onSave | `false` | *Function* | Get the wizard data on save | `(e: any) => console.log(e)` |
| onSubmit | `false` | *Function* | Get the wizard data on submit  | `(e: any) => console.log(e)` |

> **IMPORTANT:*** For savedData to work properly, the value will be passed as a 'savedValue' prop to your component. Remember to apply this using React's defaultValue prop:
```html
<input defaultValue={savedValue} />
```

### Step config
Define your wizard steps:

```js
const steps = [
  {
    entryPoint: true,
    id: 'one',
    component: Step,
    fieldIds: ['apple', 'banana'],
    next: 'two'
  },
  {
    id: 'two',
    component: Step,
    fieldIds: ['select-one', 'select-two'],
    next: [
      { fieldId: 'select-one', value: 'cat', nextId: 'three' },
      { fieldId: 'select-one', value: 'dog', nextId: 'four' }
    ]
  },
  {
    id: 'three',
    component: Step,
    fieldIds: ['mango', 'orange'],
  },
  {
    id: 'four',
    component: Step,
    fieldIds: ['pineapple'],
  }
]
```

| Option | Required | Type | Description | 
|:-|:-|:-|:-|
| entryPoint | `true` | *Boolean* | The starting step |
| id | `true` | *String* | A unique ID for this step |
| component | `true` | *ReactNode* | Your react Step component |
| fieldIds | `true` | *Array* | An array of IDs which correspond to the IDs in the field array |
| next | `true` | *String* or *Array* | The next step. An array of objects can be passed to conditionally render the next step:  **fieldId**: *String* - The id of the field to check. **value**: *Regex* - the value to match. **nextId**: *String* - If your wizard will go down one of many routes, use `nextId` to conditionally render the next step, invalidating the other routes. **skipTo**: *String* - If you want to skip steps in the journey, use skipTo instead of `nextId` to avoid this step being invalidated 

### Field config
Define your wizard fields:

```js
import Input from 'my-components/Input'

const fields = [
  {
    id: 'apple',
    component: Input,
    validation: {
      fn: (e: any) => { return e.value.length > 0 ? true : false },
      errorMsg: 'Error',
      successMsg: 'Success'
    }
  },
  {
    id: 'banana',
    component: Input
  },
  {
    id: 'select-one',
    component: Select,
    options: [
        { value: 'cat', label: 'Cat' }, 
        { value: 'dog', label: 'Dog' }
    ]
  },
  {
    id: 'select-two',
    component: Select,
    options: [
      { value: 'small', label: 'Small' },
      { value: 'medium', label: 'Medium' },
      { value: 'large', label: 'Large', prereq: { fieldId: 'select-one', value: /dog/ } },
      { value: 'tiny', label: 'Tiny', prereq: { fieldId: 'select-one', value: /cat/ } }
    ]
  },
  {
    id: 'mango',
    component: Input,
    invalidate: ['orange']
  },
  {
    id: 'orange',
    prereq: { fieldId: 'mango', value: 'orange' },
    component: Input
  },
  {
    id: 'pineapple',
    component: Input
  }
]
```
> **IMPORTANT:** In order for input values to register within the wizard state, the input value MUST be passed to the onChange function within your input component:
```html
<input onChange={(e) => onChange(e.target.value)} />
```

| Option | Required | Type | Description | 
|:-|:-|:-|:-|
| id | `true` | *String* | A unique ID for your field |
| component | `true` | *ReactNode* | Your react input component |
| hidden | `false` | *Boolean* | A hidden field is included in the state, but won't be rendered or included in the final data object |
| optional | `false` | *Boolean* | We assume that all fields are required, however set this to true to exclude the field from being validated |
| validation | `false` | *Object* | A validation object containing:  **fn**: *Function* - the function which will receive all fields state, current field Id and current field value for validating against, must return `true` or `false` as the result will be passed as the 'valid' prop to your component.  **errorMsg**: *String* - An optional error message which will be passed as a prop to your component.  **successMsg**: *String* - An optional success message which will be passed as a prop to your component |
| invalidate | `false` | *Array* | These fields will be hidden and removed from the state tree if the current field is valid. Invalidated fields will always take precedence over other conditional rendering, so consider this when structuring your form, can also be used within options |
| prereq | `false` | *Object* | This field will only show if the prerequired fieldId is valid, can also be used within options. **fieldId**: *String* - the field to check the value against. **value**: *Regex* - the value to match. |
| options | `false` | *Array* | An array of options to pass to a compatible input type such as a select:  **value**: *String* - The value of the option  **label**: *String* - The label to display of the option.  **invalidate**: *Array* - Invalidate these fields if this option is selected.  **prereq**: *Object* - Pass a **fieldId** and **value** and this option will only be shown if the prerequirement is met |


### Other configuration

When implementing your components, it is worth checking what props are passed as these can all be utilised in customising your output. For example input styles can be changed using the `valid` prop.

Enjoy!

### Testing
Run all unit and integration tests:
```sh
$ npm run test
```

Run individual tests:

| Command | Description |
|:-|:-|
| `$ npm run test:actions` | Unit test action functions |
| `$ npm run test:rendering` | Unit test rendering functions |
| `$ npm run test:components` | Unit test components |
| `$ npm run test:helpers` | Unit test helper functions |
| `$ npm run test:validation` | Unit test validation functions |
